@extends('layouts.master')

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.js"></script>
    <script>
        $(function () {
            @foreach(config('translatable.locales') as $locale)

            $('#{{ $locale }}_tree').nestable({
                maxDepth: 3,
                @isset($menu)
                json: {!! $menu->translateOrNew($locale)->children->toJson() ?? '[]' !!},
                @else
                json: [],
                @endisset
                contentCallback: function (item) {
                    return item.name || '' ? item.name : item.id;
                },
                itemRenderer: function (item_attrs, content, children, options, item) {
                    var item_attrs_string = $.map(item_attrs, function (value, key) {
                        return ' ' + key + '="' + value + '"';
                    }).join(' ');

                    var html = '<' + options.itemNodeName + item_attrs_string + '>';
                    html += '<div class="remove" data-id="' + item.id + '" data-locale="{{ $locale }}">x</div>';
                    html += '<' + options.handleNodeName + ' class="' + options.handleClass + '">';
                    html += '<' + options.contentNodeName + ' class="' + options.contentClass + '">';
                    html += content;
                    html += '</' + options.contentNodeName + '>';
                    html += '</' + options.handleNodeName + '>';
                    html += children;
                    html += '</' + options.itemNodeName + '>';

                    return html;
                },
                callback: function () {
                    {{ $locale }}_serialize();
                },
            });

            $('#{{ $locale }}_tree_input').val(JSON.stringify($('#{{ $locale }}_tree').nestable('serialize')));

            $('#{{ $locale }}_available_tree').nestable({
                callback: function () {
                    {{ $locale }}_serialize();
                },
            });

            $('#{{ $locale }}_add_button').click(function (e) {
                e.preventDefault();
                $('#{{ $locale }}_tree').nestable('add', {
                    name: $('#{{ $locale }}_add_name').val(),
                    url: $('#{{ $locale }}_add_url').val()
                });
                {{ $locale }}_serialize();
            });

            window.{{ $locale }}_serialize = function () {
                // TODO: remove after Nestable2 will be fixed
                if ($('#{{ $locale }}_tree').find('.dd-list').first().is(':empty')) {
                    $('#{{ $locale }}_tree').find('.dd-list').first().remove();
                    $('#{{ $locale }}_tree_input').val(JSON.stringify($('#{{ $locale }}_tree').nestable('serialize')));
                } else {
                    $('#{{ $locale }}_tree_input').val(JSON.stringify($('#{{ $locale }}_tree').nestable('serialize')));
                }
            };

            @endforeach

            $('.dd-item .remove').click(function () {
                $(this).parents('.dd').nestable('remove', $(this).data('id'));
                window[$(this).data('locale') + '_serialize']();
            });
        });
    </script>
@endpush

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.css" rel="stylesheet">
    <style>
        .dd-item .remove {
            position: absolute;
            top: 5px;
            right: 10px;
            color: rgba(255, 0, 0, 0.81);
            cursor: pointer;
            width: 15px;
            text-align: center;
            font-weight: bold;
        }
    </style>
@endpush


@section('content')
    <form method="POST"
          @isset($menu)
          action="{{ route('dashboard.menu.update', $menu->id) }}"
          @else
          action="{{ route('dashboard.menu.store') }}" @endisset>

        <div class="row">
            <div class="col">
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Name</label>
                    <input type="text" name="name" class="form-control col-sm-4"
                           value="{{ old('name', $menu->name ?? '') }}">
                </div>
            </div>
        </div>

        <ul class="nav nav-tabs" role="tablist">
            @foreach(config('translatable.locales') as $locale)
                <li class="nav-item">
                    <a class="nav-link @if ($loop->first) active @endif
                    @if ($errors->has('translations.'.$locale.'.*')) text-danger @endif"
                       id="{{ $locale }}-tab"
                       data-toggle="tab"
                       href="#{{ $locale }}"
                       role="tab"
                    >{{ $locale }}</a>
                </li>
            @endforeach
        </ul>

        <div class="tab-content">
            @foreach(config('translatable.locales') as $locale)
                <div class="tab-pane fade show @if ($loop->first) active @endif" id="{{ $locale }}"
                     role="tabpanel">
                    <div class="row">
                        <div class="col">
                            <h3>Current menu</h3>
                            <div class="dd" id="{{ $locale }}_tree"></div>
                        </div>
                        <div class="col">
                            <h3>Add Menu Item</h3>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="{{ $locale }}_add_name">Name</label>
                                <input type="text" class="form-control col-sm-9" id="{{ $locale }}_add_name">
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="{{ $locale }}_add_url">Url</label>
                                <input type="text" class="form-control col-sm-9" id="{{ $locale }}_add_url">
                            </div>
                            <button class="btn" id="{{ $locale }}_add_button">Add</button>
                            <hr>
                            <h3>Available items</h3>
                            <div class="dd" id="{{ $locale }}_available_tree">
                                <ol class="dd-list">
                                    @foreach(config('menu.itemable') as $itemable)
                                        @if($itemable['translatable'] ?? false)
                                            @foreach($itemable['class']::translatedIn($locale)->get() as $item)
                                                <li class="dd-item"
                                                    data-itemable_type="{{ $itemable['class'] }}"
                                                    data-itemable_id="1"
                                                    data-name="{{ $item->translate($locale)->{$itemable['name_field']} }}"
                                                >
                                                    <div class="dd-handle">
                                                        {{ $item->translate($locale)->{$itemable['name_field']} }}
                                                    </div>
                                                </li>
                                            @endforeach
                                        @else
                                            @foreach($itemable['class']::all() as $item)
                                                <li class="dd-item"
                                                    data-itemable_type="{{ $itemable['class'] }}"
                                                    data-itemable_id="1"
                                                    data-name="{{ $item->translate($locale)->{$itemable['name_field']} }}"
                                                >
                                                    <div class="dd-handle">
                                                        {{ $item->{$itemable['name_field']} }}
                                                    </div>
                                                </li>
                                            @endforeach
                                        @endif
                                    @endforeach
                                </ol>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="{{ $locale }}[tree]" id="{{ $locale }}_tree_input">
                </div>
            @endforeach
        </div>
        {{ csrf_field() }}
        @isset($menu)
            {{ method_field('PATCH') }}
        @endisset
        <button class="btn" id="save_menu">Save</button>
    </form>
@endsection