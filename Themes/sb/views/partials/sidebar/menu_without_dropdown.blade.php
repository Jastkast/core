<li class="nav-item {{ $item->isActive() ? ' active' : '' }}" data-toggle="tooltip" data-placement="right" title="{{ $item->title }}">
    <a class="nav-link" href="{{ $item->getUrl() }}">
        <i class="fa fa-fw fa-{{ $item->icon }}"></i>
        <span class="nav-link-text">{{ $item->title }}</span>
    </a>
</li>