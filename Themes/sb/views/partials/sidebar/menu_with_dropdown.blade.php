<li class="nav-item">
    <a class="nav-link nav-link-collapse {{ $active_state ? '' : 'collapsed' }}" data-toggle="collapse" href="#{{ $id }}">
        <i class="fa fa-fw fa-{{ $item->icon }}"></i>
        <span class="nav-link-text">{{ $item->title }}</span>
    </a>
    <ul class="sidenav-second-level collapse {{ $active_state ? 'show' : '' }}" id="{{ $id }}">
        {!! $child !!}
    </ul>
</li>