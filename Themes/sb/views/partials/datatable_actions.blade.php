<div class="btn-group" role="group">
    <a href="{{ route($route_edit, ['id' => $item->id]) }}" class="btn btn-primary">
        <i class="fa fa-edit"></i></a>
    <a data-method="delete" href="{{ route($route_delete, ['id' => $item->id]) }}" class="btn btn-danger">
        <i class="fa fa-remove"></i>
    </a>
</div>