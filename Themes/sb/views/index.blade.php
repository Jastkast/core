@extends('layouts.master')

@section('content')
    <h1>
        The trouble with having an open mind, of course, is that people will insist on coming along and trying to put
        things in it.
    </h1>
    <ul>
        <caption>
            Things to do:
        </caption>
        <li>Module generator</li>
        <li>Media module</li>
        <li>React front</li>
        <li>Pages module</li>
        <li>Breadcrumbs</li>
    </ul>
@endsection
