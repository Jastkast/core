@extends('layouts.master')

@section('content')
    {!! $dataTable->table() !!}
@endsection

@push('styles')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
{{--<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.bootstrap4.min.css">--}}
@endpush

@push('scripts')

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.bootstrap4.min.js"></script>

<script src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}

<script>
    (function () {

        var laravel = {
            initialize: function () {
                this.selector = 'a[data-method]';

                this.registerEvents();
            },

            registerEvents: function () {
                // TODO: fix this
                var $table = $('table#dataTableBuilder').DataTable();
                var that = this;

                $table.on('draw.dt', function () {
                    $table.on('click', that.selector, that.handleMethod);
                });
            },

            handleMethod: function (e) {
              var link = $(e.target);
                var form;

                if (link.data('confirm')) {
                    if (!laravel.verifyConfirm(link)) {
                        return false;
                    }
                }

                form = laravel.createForm(link);
                form.submit();

                e.preventDefault();
                return false;
            },

            verifyConfirm: function (link) {
                return confirm(link.data('confirm'));
            },

            createForm: function (link) {
                var form =
                    $('<form>', {
                        'method': 'POST',
                        'action': link.attr('href')
                    });

                var token =
                    $('<input>', {
                        'type': 'hidden',
                        'name': '_token',
                        'value': '{{  csrf_token() }}'
                    });

                var hiddenInput =
                    $('<input>', {
                        'name': '_method',
                        'type': 'hidden',
                        'value': link.data('method')
                    });

                return form.append(token, hiddenInput)
                    .appendTo('body');
            }
        };
        laravel.initialize();
    })();
</script>
@endpush