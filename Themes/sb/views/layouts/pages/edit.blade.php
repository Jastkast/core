@extends('layouts.master')

@section('content')
    {!! form_start($form) !!}
    @if($form::TRANSLATABLE)
        <ul class="nav nav-tabs" role="tablist">
            @foreach(config('translatable.locales') as $locale)
                <li class="nav-item">
                    <a class="nav-link @if ($loop->first) active @endif
                    @if ($errors->has('translations.'.$locale.'.*')) text-danger @endif"
                       id="{{ $locale }}-tab"
                       data-toggle="tab"
                       href="#{{ $locale }}"
                       role="tab"
                    >{{ $locale }}</a>
                </li>
            @endforeach
        </ul>

        <div class="tab-content">
            @foreach(config('translatable.locales') as $locale)
                <div class="tab-pane fade show @if ($loop->first) active @endif" id="{{ $locale }}" role="tabpanel">
                    {!! form_row($form->{'translations['.$locale.']'}) !!}
                </div>
            @endforeach
        </div>
    @endif

    {!! form_end($form, true) !!}
@endsection

@push('scripts')
    @include('partials.tinymce')
@endpush