<?php

namespace Modules\Core\Forms;

use Kris\LaravelFormBuilder\Form;

abstract class CoreForm extends Form
{
    const TRANSLATABLE = false;

    public function buildForm() {
        if ($this->is_create && method_exists($this, 'createForm')) {
            return $this->createForm();
        } elseif ($this->is_update) {
            return $this->updateForm();
        }

        return $this->form();
    }

    public function getType() {
        if (!strcasecmp($this->getMethod(), 'POST')) {
            return 'create';
        } elseif (!strcasecmp($this->getMethod(), 'PATCH') || !strcasecmp($this->getMethod(), 'PUT')) {
            return 'update';
        }
        return null;
    }

    public function __get($name) {
        if ($name === 'is_create') {
            return $this->getType() === 'create';
        } elseif ($name === 'is_update') {
            return $this->getType() === 'update';
        }
        return parent::__get($name);
    }
}
