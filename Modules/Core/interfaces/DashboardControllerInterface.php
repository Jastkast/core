<?php
namespace Modules\Core\Interfaces;

use Illuminate\Http\Request;

interface DashboardControllerInterface
{
    public function login();

    public function authenticate(Request $request);

    public function logout();
}
