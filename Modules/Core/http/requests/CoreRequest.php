<?php

namespace Modules\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class CoreRequest extends FormRequest
{
    public function authorize() {
        return true;
    }

    public function rules() {
        if ($this->is_create) {
            return $this->createRules();
        } elseif ($this->is_update) {
            return $this->updateRules();
        } else {
            abort(500, 'Form action was not correctly implemented.');
        }
    }

    public function initialize(
        array $query = [],
        array $request = [],
        array $attributes = [],
        array $cookies = [],
        array $files = [],
        array $server = [],
        $content = null
    ) {
        parent::initialize($query, $request, $attributes, $cookies, $files, $server, $content);

        $data = $this->getInputSource()->all();
        $data = $this->modifyInput($data);
        $this->getInputSource()->replace($data);
    }

    public function modifyInput($data) {
        return $data;
    }

    public function getType() {
        if (!strcasecmp($this->method(), 'POST')) {
            return 'create';
        } elseif (!strcasecmp($this->method(), 'PATCH') || !strcasecmp($this->method(), 'PUT')) {
            return 'update';
        }
        return null;
    }

    public function __get($name) {
        if ($name === 'is_create') {
            return $this->getType() === 'create';
        } elseif ($name === 'is_update') {
            return $this->getType() === 'update';
        }

        return parent::__get($name);
    }
}
