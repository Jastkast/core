<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Routing\Controller;
use Kris\LaravelFormBuilder\FormBuilderTrait;

abstract class CoreDashboardController extends Controller
{
    use FormBuilderTrait;
}
