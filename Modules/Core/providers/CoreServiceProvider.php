<?php

namespace Modules\Core\Providers;

use Modules\Core\Commands\ClearDB;
use Modules\Core\Commands\Reset;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\Core\Console\Commands\ModuleMakeCommand;

class CoreServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot() {
        $this->registerConfig();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
        $this->registerDashboardSidebar();

        $this->commands([
            Reset::class,
            ClearDB::class,
            ModuleMakeCommand::class,
        ]);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig() {
        $this->publishes([
            __DIR__ . '/../config/config.php' => config_path('core.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__ . '/../config/config.php',
            'core'
        );
    }

    public function register() {
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides() {
        return [];
    }

    public function registerDashboardSidebar() {
        \Menu::create('navbar', function ($menu) {
            foreach (\Module::collections() as $module) {
                $nav = config($module->getLowerName() . '.navigation');

                if (!$nav) {
                    continue;
                }

                foreach ($nav as $el) {
                    if (isset($el['children'])) {
                        $menu->dropdown($el['title'], function ($sub) use ($el) {
                            foreach ($el['children'] as $child) {
                                if (isset($child['link'])) {
                                    $sub->url($child['link'], $child['title'], $child['attributes'] ?? []);
                                } elseif (isset($child['route'])) {
                                    $sub->route(
                                        $child['route'],
                                        $child['title'],
                                        $child['parameters'] ?? [],
                                        $child['attributes'] ?? []
                                    );
                                }
                            }
                        }, $el['attributes'] ?? []);
                    } else {
                        if (isset($el['link'])) {
                            $menu->url($el['link'], $el['title'], $el['attributes'] ?? []);
                        } elseif (isset($el['route'])) {
                            $menu->route($el['route'], $el['title'], [], $el['attributes'] ?? []);
                        }
                    }
                }
            }
        });
    }
}
