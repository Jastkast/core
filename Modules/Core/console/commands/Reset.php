<?php

namespace Modules\Core\Commands;

use Illuminate\Console\Command;

class Reset extends Command
{

    protected $signature = 'reset';
    protected $description = 'Resetting system';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $this->info('-----------Clearing database-----------');
        $this->call('db:clear');

        $this->info('-----------Running migrations-----------');
        $this->call('module:migrate');

        $this->info('-----------Seeding database-----------');
        $this->call('module:seed');

        $this->info('-----------Clearing cache----------');
        $this->call('cache:clear');
    }
}
