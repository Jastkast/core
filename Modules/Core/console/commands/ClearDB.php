<?php

namespace Modules\Core\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ClearDB extends Command
{

    protected $signature = 'db:clear';

    protected $description = 'Clear Database';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        if (env('APP_ENV') !== 'local') {
            exit('Do NOT run this on production!' . PHP_EOL);
        }
        Schema::disableForeignKeyConstraints();

        $tables = [];
        foreach (DB::select(
            'SELECT table_name FROM information_schema.tables WHERE table_schema="' . env('DB_DATABASE') . '"'
        ) as $k => $v) {
            $tables[] = array_values((array)$v)[0];
        }

        foreach ($tables as $table) {
            DB::statement("DROP TABLE $table CASCADE");
            echo "Table " . $table . " has been dropped." . PHP_EOL;
        }

        Schema::enableForeignKeyConstraints();
    }
}
