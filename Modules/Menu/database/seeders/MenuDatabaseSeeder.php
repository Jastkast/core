<?php

namespace Modules\Menu\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Core\Models\Permission;
use Modules\Menu\Models\Menu;
use Modules\Menu\Models\MenuItem;
use Faker\Generator as Faker;
use Modules\Menu\Models\MenuTranslation;
use Modules\Page\Models\Page;

class MenuDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Model::unguard();

        Permission::createResource('dashboard menu');

        $menu = Menu::create([
            'name' => 'First',
        ]);

        $translation = MenuTranslation::create([
            'locale' => 'en',
            'menu_id' => $menu->id
        ]);

        $translation->children()->save(
            new MenuItem([
                'name' => 'Root',
                'itemable_id' => 1,
                'itemable_type' => Page::class
            ])
        );

        $translation->children->first()->children()->saveMany([
            new MenuItem(['name' => 'Sub First']),
            new MenuItem(['name' => 'Sub Second']),
            new MenuItem(['name' => 'Sub Three']),
        ]);
    }
}
