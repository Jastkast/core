<?php

namespace Modules\Menu\Models;

use Illuminate\Database\Eloquent\Model;

class MenuTranslation extends Model
{

    protected $fillable = [
        'locale',
        'menu_id'
    ];

    public $with = ['children'];

    public $timestamps = false;

    public function children() {
        return $this->hasMany(MenuItem::class)->whereNull('parent_id');
    }

    public function rebuild($tree, $root = null) {
        $root = $root ?? $this;
        foreach ($tree as $index => $item) {
            $menu_item = MenuItem::create([
                'name' => $item['name'],
                'order' => $index,
                'menu_translation_id' => $this->id,
                'itemable_id' => $item['itemable_id'] ?? null,
                'itemable_type' => $item['itemable_type'] ?? null,
            ]);

            $root->children()->save(
                $menu_item
            );

            if (isset($item['children'])) {
                $this->rebuild($item['children'], $menu_item);
            }
        }
    }
}
