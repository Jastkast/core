<?php

namespace Modules\Menu\Models;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{

    protected $fillable = [
        'name',
        'menu_translation_id',
        'order',
        'itemable_id',
        'itemable_type',
    ];

    public $timestamps = false;

    public $with = ['children', 'itemable'];

    public function children() {
        return $this->hasMany(MenuItem::class, 'parent_id')->orderBy('order');
    }

    public function itemable() {
        return $this->morphTo();
    }
}
