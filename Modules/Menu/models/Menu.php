<?php

namespace Modules\Menu\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use Translatable;

    protected $fillable = [
        'name'
    ];

    public $translatedAttributes = [];

    public $with = ['translations', 'translations.children'];

    public function children() {
        return $this->hasManyThrough(MenuItem::class, MenuTranslation::class)->orderBy('order');
    }
}
