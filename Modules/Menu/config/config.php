<?php

return [
    'name' => 'Menu',
    'navigation' => [
        [
            'title' => 'Menus',
            'attributes' => [
                'icon' => 'cog'
            ],
            'children' => [
                [
                    'title' => 'Menus List',
                    'route' => 'dashboard.menu.index'
                ]
            ]
        ]
    ],
    'itemable' => [
        [
            'class' => \Modules\Page\Models\Page::class,
            'name_field' => 'title',
            'translatable' => true
        ]
    ]
];
