<?php

namespace Modules\Menu\Forms;

use Modules\Core\Forms\CoreForm;

class MenuForm extends CoreForm
{
    public function createForm() {
        $this->add('name', 'text')
            ->add('submit', 'submit', ['label' => 'Save']);
    }

    public function updateForm() {
        $this->add('name', 'text')
            ->add('submit', 'submit', ['label' => 'Update']);
    }
}
