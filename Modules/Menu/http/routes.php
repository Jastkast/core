<?php

Route::group(
    [
        'middleware' => ['web', 'theme:' . config('theme.front_theme')],
        'prefix' => 'menu',
        'namespace' => 'Modules\Menu\Http\Controllers'
    ],
    function () {
        Route::get('/', 'MenuController@index');
    }
);

Route::group(
    [
        'middleware' => ['web', 'theme:' . config('theme.back_theme')],
        'prefix' => '/dashboard/menu',
        'namespace' => 'Modules\Menu\Http\Controllers'
    ],
    function () {
        Route::get('/', [
            'middleware' => 'permission:dashboard.menu.index',
            'as' => 'dashboard.menu.index',
            'uses' => 'MenuDashboardController@index'
        ]);

        Route::get('/create', [
            'middleware' => 'permission:dashboard.menu.create',
            'as' => 'dashboard.menu.create',
            'uses' => 'MenuDashboardController@create'
        ]);

        Route::post('/', [
            'middleware' => 'permission:dashboard.menu.create',
            'as' => 'dashboard.menu.store',
            'uses' => 'MenuDashboardController@store'
        ]);

        Route::get('{menu}/edit', [
            'middleware' => 'permission:dashboard.menu.update',
            'as' => 'dashboard.menu.edit',
            'uses' => 'MenuDashboardController@edit'
        ]);

        Route::patch('{menu}', [
            'middleware' => 'permission:dashboard.menu.update',
            'as' => 'dashboard.menu.update',
            'uses' => 'MenuDashboardController@update'
        ]);

        Route::delete('{menu}', [
            'middleware' => 'permission:dashboard.menu.delete',
            'as' => 'dashboard.menu.delete',
            'uses' => 'MenuDashboardController@destroy'
        ]);
    }
);
