<?php

namespace Modules\Menu\Http\Requests;

use Modules\Core\Http\Requests\CoreRequest;

class MenuRequest extends CoreRequest
{
    public function rules() {
        return [
            'name' => 'required',
        ];
    }
}
