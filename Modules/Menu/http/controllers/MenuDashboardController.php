<?php

namespace Modules\Menu\Http\Controllers;

use Modules\Core\Http\Controllers\CoreDashboardController;
use Modules\Menu\DataTables\MenuDataTable;
use Modules\Menu\Http\Requests\MenuRequest;
use Modules\Menu\Models\Menu;
use Modules\Menu\Models\MenuTranslation;

class MenuDashboardController extends CoreDashboardController
{

    public function index(MenuDataTable $dataTable) {
        return $dataTable->render('layouts.datatable');
    }

    public function create() {
        return view('modules.menu.create_edit');
    }

    public function store(MenuRequest $request) {
        $menu = Menu::create($request->only('name'));
        foreach (config('translatable.locales') as $locale) {
            $translation = $menu->translations()->save(
                new MenuTranslation([
                    'locale' => $locale,
                    'menu_id' => $menu->id
                ])
            );
            $translation->rebuild(json_decode($request->{$locale}['tree'], true));
        }
        return redirect()->back();
    }

    public function edit(Menu $menu) {
        return view('modules.menu.create_edit', compact('menu'));
    }

    public function update(Menu $menu, MenuRequest $request) {
        $menu->update($request->only('name'));
        $menu->children()->delete();
        foreach (config('translatable.locales') as $locale) {
            $translation = $menu->getTranslation($locale);
            if (!$translation) {
                $translation = $menu->translations()->save(
                    new MenuTranslation([
                        'locale' => $locale,
                        'menu_id' => $menu->id
                    ])
                );
            }

            $translation->rebuild(json_decode($request->{$locale}['tree'], true));
        }
        return redirect()->back();
    }

    public function destroy(Menu $menu) {
        $menu->delete();
        return back();
    }
}
