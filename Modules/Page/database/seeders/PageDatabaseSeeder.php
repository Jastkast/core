<?php

namespace Modules\Page\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Models\Permission;
use Modules\Page\Models\Page;

class PageDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Model::unguard();

        Permission::createResource('dashboard page');

        $page = Page::create([]);
        $page->fill([
            'title:pl' => 'Tytuł',
            'content:pl' => 'Treść',
            'route:pl' => 'page',
            'title:en' => 'Sample title',
        ])->save();
    }
}
