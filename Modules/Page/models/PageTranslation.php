<?php

namespace Modules\Page\Models;

use Illuminate\Database\Eloquent\Model;

class PageTranslation extends Model
{
    protected $fillable = [
        'title',
        'route',
        'content',
    ];

    public $timestamps = false;
}
