<?php

namespace Modules\Page\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use Translatable;

    protected $fillable = [
        'translations'
    ];

    public $translatedAttributes = [
        'title',
        'route',
        'content',
    ];

    public function setTranslationsAttribute($translations) {
        $this->fill($translations);
    }
}
