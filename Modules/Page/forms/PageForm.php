<?php

namespace Modules\Page\Forms;

use Modules\Core\Forms\CoreForm;

class PageForm extends CoreForm
{
    const TRANSLATABLE = true;

    public function createForm() {
        foreach (config('translatable.locales') as $locale) {
            $this->add('translations[' . $locale . ']', 'form', [
                'class' => PageTranslationForm::class,
                'label' => false
            ]);
        }

        $this->add('submit', 'submit', ['label' => 'Save']);
    }

    public function updateForm() {
        foreach (config('translatable.locales') as $locale) {
            $this->add('translations[' . $locale . ']', 'form', [
                'class' => PageTranslationForm::class,
                'value' => $this->getModel()->getTranslation($locale),
                'label' => false
            ]);
        }

        $this->add('submit', 'submit', ['label' => 'Update']);
    }
}
