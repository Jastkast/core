<?php

namespace Modules\Page\Forms;

use Modules\Core\Forms\CoreForm;

class PageTranslationForm extends CoreForm
{
    public function form() {
        $this->add('title', 'text')
            ->add('route', 'text')
            ->add('content', 'textarea', ['attr' => ['class' => 'rich form-control']]);
    }
}
