<?php

return [
    'name' => 'Pages',
    'navigation' => [
        [
            'title' => 'Pages',
            'attributes' => [
                'icon' => 'file-text'
            ],
            'children' => [
                [
                    'title' => 'Pages List',
                    'route' => 'dashboard.pages.index'
                ]
            ]
        ]
    ]
];
