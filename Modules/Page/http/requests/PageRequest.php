<?php

namespace Modules\Page\Http\Requests;

use Modules\Core\Http\Requests\CoreRequest;

class PageRequest extends CoreRequest
{
    public function rules() {
        return [
            'translations.*.title' => 'required',
        ];
    }
}
