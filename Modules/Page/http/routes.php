<?php

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['web', 'theme:' . config('theme.front_theme')],
        'namespace' => 'Modules\Page\Http\Controllers',
    ],
    function () {
        Route::bind('route', function ($value) {
            return \Modules\Page\Models\Page::whereHas('translations', function ($q) use ($value) {
                $q->whereRoute($value);
            })->first() ?? abort(404);
        });

        Route::get('/{route}', [
            'as' => 'page.page',
            'uses' => 'PageController@page',
        ])->setPriority(0);
    }
);

Route::group(
    [
        'middleware' => ['web', 'theme:' . config('theme.back_theme')],
        'prefix' => '/dashboard/pages',
        'namespace' => 'Modules\Page\Http\Controllers'
    ],
    function () {
        Route::get('/', [
            'middleware' => 'permission:dashboard.page.index',
            'as' => 'dashboard.pages.index',
            'uses' => 'PageDashboardController@index'
        ]);

        Route::get('/create', [
            'middleware' => 'permission:dashboard.page.create',
            'as' => 'dashboard.pages.create',
            'uses' => 'PageDashboardController@create'
        ]);

        Route::post('/', [
            'middleware' => 'permission:dashboard.page.create',
            'as' => 'dashboard.pages.store',
            'uses' => 'PageDashboardController@store'
        ]);

        Route::get('{page}/edit', [
            'middleware' => 'permission:dashboard.page.update',
            'as' => 'dashboard.pages.edit',
            'uses' => 'PageDashboardController@edit'
        ]);

        Route::patch('{page}', [
            'middleware' => 'permission:dashboard.page.update',
            'as' => 'dashboard.pages.update',
            'uses' => 'PageDashboardController@update'
        ]);

        Route::delete('{page}', [
            'middleware' => 'permission:dashboard.page.delete',
            'as' => 'dashboard.pages.delete',
            'uses' => 'PageDashboardController@destroy'
        ]);
    }
);
