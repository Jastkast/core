<?php

namespace Modules\Page\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Page\Models\Page;

class PageController extends Controller
{
    public function page(Page $page) {
        return view('page', compact('page'));
    }
}
