<?php

namespace Modules\Page\Http\Controllers;

use Modules\Core\Http\Controllers\CoreDashboardController;
use Modules\Page\DataTables\PageDataTable;
use Modules\Page\Forms\PageForm;
use Modules\Page\Http\Requests\PageRequest;
use Modules\Page\Models\Page;

class PageDashboardController extends CoreDashboardController
{

    public function index(PageDataTable $dataTable) {
        return $dataTable->render('layouts.datatable');
    }

    public function create() {
        $form = $this->form(PageForm::class, [
            'url' => route('dashboard.pages.store'),
            'method' => 'POST'
        ]);
        return view('layouts.pages.create', compact('form'));
    }

    public function store(PageRequest $request) {
        Page::create($request->all());
        return redirect()->back();
    }

    public function edit(Page $page) {
        $form = $this->form(PageForm::class, [
            'model' => $page,
            'url' => route('dashboard.pages.update', $page->id),
            'method' => 'PATCH'
        ]);
        return view('layouts.pages.edit', compact('form'));
    }

    public function update(Page $page, PageRequest $request) {
        $page->update($request->all());
        return redirect()->back();
    }

    public function destroy(Page $page) {
        $page->delete();
        return back();
    }
}
