<?php

namespace Modules\Dashboard\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Models\Permission;

class PermissionsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Model::unguard();

        Permission::create([
                'slug' =>       'dashboard.index',
                'resource' =>   'Dashboard',
                'name' =>       'View Dashboard',
                'system' =>     0,
            ]);
    }
}
