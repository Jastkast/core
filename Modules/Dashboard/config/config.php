<?php

return [
    'name' => 'Dashboard',
    'navigation' => [
        [
            'title' => 'Dashboard',
            'link' => '/dashboard',
            'attributes' => [
                'icon' => 'dashboard'
            ]
        ]
    ]
];
