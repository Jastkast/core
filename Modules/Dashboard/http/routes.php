<?php

Route::group([
    'middleware' => ['web', 'theme:'.config('theme.back_theme')],
    'prefix' => 'dashboard',
    'namespace' => 'Modules\Dashboard\Http\Controllers'
], function () {
    Route::get('/', [
        'as' => 'dashboard.index',
        'uses' => 'DashboardController@index',
        'middleware' => ['auth', 'permission:dashboard.index']
    ]);

    Route::get('login', [
        'as' => 'login',
        'uses' => 'DashboardController@login',
    ]);

    Route::post('authenticate', [
        'as' => 'authenticate',
        'uses' => 'DashboardController@authenticate',
    ]);

    Route::get('logout', [
        'as' => 'dashboard.logout',
        'uses' => 'DashboardController@logout',
    ]);
});
