<?php

namespace Modules\Dashboard\Http\Controllers;

use Modules\Core\Http\Controllers\CoreDashboardController;
use Modules\Core\Interfaces\DashboardControllerInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends CoreDashboardController implements DashboardControllerInterface
{

    public function index() {
        return view('sb::index');
    }

    public function login() {
        return view('sb::login');
    }

    public function authenticate(Request $request) {
        if (Auth::attempt([
            'email' => $request->email,
            'password' => $request->password
        ], $request->remember_me ?? false)
        ) {
            return redirect()->route('dashboard.index');
        }
        // TODO: make some validation
        return redirect()->back();
    }

    public function logout() {
        auth()->logout();
        return redirect('/');
    }
}
