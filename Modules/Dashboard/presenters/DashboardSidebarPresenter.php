<?php

namespace Modules\Dashboard\Presenters;

use Nwidart\Menus\Presenters\Presenter;

class DashboardSidebarPresenter extends Presenter
{
    /**
     * Get open tag wrapper.
     *
     * @return string
     */
    public function getOpenTagWrapper() {
        return view('partials.sidebar.open_tag');
    }

    /**
     * Get close tag wrapper.
     *
     * @return string
     */
    public function getCloseTagWrapper() {
        return view('partials.sidebar.close_tag');
    }

    /**
     * Get menu tag without dropdown wrapper.
     *
     * @param \Nwidart\Menus\MenuItem $item
     *
     * @return string
     */
    public function getMenuWithoutDropdownWrapper($item) {
        return view('partials.sidebar.menu_without_dropdown', ['item' => $item]);
    }

    public function getActiveStateOnChild($item, $state = 'active') {
        return $item->hasActiveOnChild() ? $state : null;
    }

    public function getDividerWrapper() {
        return view('partials.sidebar.divider');
    }

    public function getHeaderWrapper($item) {
        return view('partials.sidebar.header', ['item' => $item]);
    }

    public function getMenuWithDropDownWrapper($item) {
        $id = str_random(4);
        return view('partials.sidebar.menu_with_dropdown', [
            'item' => $item,
            'id' => $id,
            'active_state' => $this->getActiveStateOnChild($item),
            'child' => $this->getChildMenuItems($item),
        ]);
    }

    public function getMultiLevelDropdownWrapper($item) {
        return $this->getMenuWithDropDownWrapper($item);
    }
}
