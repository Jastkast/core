<?php

return [
    'name' => 'User',
    'navigation' => [
        [
            'title' => 'Users',
            'attributes' => [
                'icon' => 'user'
            ],
            'children' => [
                [
                    'title' => 'Users List',
                    'route' => 'dashboard.users.index'
                ],
                [
                    'title' => 'Roles List',
                    'route' => 'dashboard.users.roles.index'
                ],
                [
                    'title' => 'Permissions List',
                    'route' => 'dashboard.users.permissions.index'
                ]
            ]
        ]
    ]
];
