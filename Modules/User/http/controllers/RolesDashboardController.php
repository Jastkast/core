<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\CoreDashboardController;
use Modules\Core\Models\Role;
use Modules\User\DataTables\RoleDataTable;
use Modules\User\Forms\RoleForm;
use Modules\User\Http\Requests\RoleRequest;

class RolesDashboardController extends CoreDashboardController
{

    public function index(RoleDataTable $dataTable) {
        return $dataTable->render('layouts.datatable');
    }

    public function create() {
        $form = $this->form(RoleForm::class, [
            'url' => route('dashboard.users.roles.store'),
            'method' => 'POST'
        ]);
        return view('layouts.pages.create', compact('form'));
    }

    public function store(Request $request) {
        $role = Role::create($request->all());
        $role->syncPermissions(collect($request->permissions)->collapse()->toArray());
        return redirect()->back();
    }

    public function show() {
        return view('user::show');
    }

    public function edit(Role $role) {
        $form = $this->form(RoleForm::class, [
            'model' => $role,
            'url' => route('dashboard.users.roles.update', $role->id),
            'method' => 'PATCH'
        ]);
        return view('layouts.pages.edit', compact('form'));
    }

    public function update(Role $role, RoleRequest $request) {
        $role->update($request->all());
        $role->syncPermissions(collect($request->permissions)->collapse()->toArray());
        return redirect()->back();
    }

    public function destroy() {
    }
}
