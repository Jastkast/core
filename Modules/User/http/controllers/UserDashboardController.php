<?php

namespace Modules\User\Http\Controllers;

use Modules\Core\Http\Controllers\CoreDashboardController;
use Modules\User\DataTables\UserDataTable;
use Modules\User\Forms\UserForm;
use Modules\User\Http\Requests\UserRequest;
use Modules\User\Models\User;

class UserDashboardController extends CoreDashboardController
{

    public function index(UserDataTable $dataTable) {
        return $dataTable->render('layouts.datatable');
    }

    public function create() {
        $form = $this->form(UserForm::class, [
            'url' => route('dashboard.users.store'),
            'method' => 'POST'
        ]);
        return view('layouts.pages.create', compact('form'));
    }

    public function store(UserRequest $request) {
        User::create($request->all())
            ->roles()->sync($request->role_id);
        return redirect()->back();
    }

    public function edit(User $user) {
        $form = $this->form(UserForm::class, [
            'model' => $user,
            'url' => route('dashboard.users.update', $user->id),
            'method' => 'PATCH'
        ]);
        return view('layouts.pages.edit', compact('form'));
    }

    public function update(User $user, UserRequest $request) {
        $user->update($request->all());
        $user->roles()->sync($request->role_id);
        return redirect()->back();
    }

    public function destroy(User $user) {
        $user->delete();
        return back();
    }
}
