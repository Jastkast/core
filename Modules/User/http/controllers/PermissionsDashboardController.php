<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\CoreDashboardController;
use Modules\User\DataTables\PermissionDataTable;
use Modules\User\Forms\PermissionForm;

class PermissionsDashboardController extends CoreDashboardController
{

    public function index(PermissionDataTable $dataTable) {
        return $dataTable->render('layouts.datatable');
    }

    public function create() {
        $form = $this->form(PermissionForm::class, [
            'url' => route('dashboard.users.permissions.store'),
            'method' => 'POST'
        ]);
        return view('layouts.pages.create', compact('form'));
    }

    public function store(Request $request) {
    }

    public function show() {
        return view('user::show');
    }

    public function edit() {
        return view('user::edit');
    }

    public function update(Request $request) {
    }

    public function destroy() {
    }
}
