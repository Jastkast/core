<?php

use Illuminate\Support\Facades\Route;

Route::group(
    [
        'middleware' => ['web', 'theme:'.config('theme.back_theme')],
        'prefix' => '/dashboard/users',
        'namespace' => 'Modules\User\Http\Controllers'
    ],
    function () {
        Route::get('/', [
            'middleware' => 'permission:dashboard.users.index',
            'as' => 'dashboard.users.index',
            'uses' => 'UserDashboardController@index'
        ]);

        Route::get('/create', [
            'middleware' => 'permission:dashboard.users.create',
            'as' => 'dashboard.users.create',
            'uses' => 'UserDashboardController@create'
        ]);

        Route::post('/', [
            'middleware' => 'permission:dashboard.users.create',
            'as' => 'dashboard.users.store',
            'uses' => 'UserDashboardController@store'
        ]);

        Route::get('{user}/edit', [
            'middleware' => 'permission:dashboard.users.update',
            'as' => 'dashboard.users.edit',
            'uses' => 'UserDashboardController@edit'
        ]);

        Route::patch('{user}', [
            'middleware' => 'permission:dashboard.users.update',
            'as' => 'dashboard.users.update',
            'uses' => 'UserDashboardController@update'
        ]);

        Route::delete('{user}', [
            'middleware' => 'permission:dashboard.users.delete',
            'as' => 'dashboard.users.delete',
            'uses' => 'UserDashboardController@destroy'
        ]);


        // ROLES

        Route::get('/roles', [
            'middleware' => 'permission:dashboard.users.roles.index',
            'as' => 'dashboard.users.roles.index',
            'uses' => 'RolesDashboardController@index'
        ]);

        Route::get('/roles/create', [
            'middleware' => 'permission:dashboard.users.roles.create',
            'as' => 'dashboard.users.roles.create',
            'uses' => 'RolesDashboardController@create'
        ]);

        Route::post('/roles', [
            'middleware' => 'permission:dashboard.users.roles.create',
            'as' => 'dashboard.users.roles.store',
            'uses' => 'RolesDashboardController@store'
        ]);

        Route::get('/roles/{role}/edit', [
            'middleware' => 'permission:dashboard.users.roles.update',
            'as' => 'dashboard.users.roles.edit',
            'uses' => 'RolesDashboardController@edit'
        ]);

        Route::patch('/roles/{role}', [
            'middleware' => 'permission:dashboard.users.roles.update',
            'as' => 'dashboard.users.roles.update',
            'uses' => 'RolesDashboardController@update'
        ]);

        Route::delete('/roles/{role}', [
            'middleware' => 'permission:dashboard.users.roles.delete',
            'as' => 'dashboard.users.roles.delete',
            'uses' => 'RolesDashboardController@delete'
        ]);

        // PERMISSIONS

        Route::get('/permissions', [
            'middleware' => 'permission:dashboard.users.permissions.index',
            'as' => 'dashboard.users.permissions.index',
            'uses' => 'PermissionsDashboardController@index'
        ]);

        Route::get('/permissions/create', [
            'middleware' => 'permission:dashboard.users.permissions.create',
            'as' => 'dashboard.users.permissions.create',
            'uses' => 'PermissionsDashboardController@create'
        ]);

        Route::post('/permissions', [
            'middleware' => 'permission:dashboard.users.permissions.create',
            'as' => 'dashboard.users.permissions.store',
            'uses' => 'PermissionsDashboardController@store'
        ]);

        Route::get('/permissions/{permission}/edit', [
            'middleware' => 'permission:dashboard.users.permissions.update',
            'as' => 'dashboard.users.permissions.edit',
            'uses' => 'PermissionsDashboardController@edit'
        ]);

        Route::patch('/permissions/{permission}', [
            'middleware' => 'permission:dashboard.users.permissions.update',
            'as' => 'dashboard.users.permissions.update',
            'uses' => 'PermissionsDashboardController@update'
        ]);

        Route::delete('/permissions/{permission}', [
            'middleware' => 'permission:dashboard.users.permissions.delete',
            'as' => 'dashboard.users.permissions.delete',
            'uses' => 'PermissionsDashboardController@delete'
        ]);
    }
);
