<?php

namespace Modules\User\Http\Requests;

use Modules\Core\Http\Requests\CoreRequest;

class RoleRequest extends CoreRequest
{
    public function rules() {
        return [
            'name' => 'required',
        ];
    }
}
