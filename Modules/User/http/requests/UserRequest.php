<?php

namespace Modules\User\Http\Requests;

use Illuminate\Validation\Rule;
use Modules\Core\Http\Requests\CoreRequest;

class UserRequest extends CoreRequest
{
    public function createRules() {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6',
        ];
    }

    public function updateRules() {
        return [
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('users')->ignore($this->user->id, 'id')],
            'password' => 'nullable|confirmed|min:6',
        ];
    }

    public function modifyInput($data) {
        if ($this->is_update && key_exists('password', $data) && !isset($data['password'])) {
            unset($data['password']);
        }
        return $data;
    }
}
