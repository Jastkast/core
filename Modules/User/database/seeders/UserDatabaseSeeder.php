<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\User\Models\User;

class UserDatabaseSeeder extends Seeder
{
    public function run() {
        Model::unguard();

        $user = User::create([
            'name' => 'admin',
            'email' => 'admin@cms.com',
            'password' => '@dm1N',
        ]);

        $this->call(RolesDatabaseSeeder::class);
        $user->attachRoleBySlug('super_admin');

        $this->call(PermissionsDatabaseSeeder::class);
    }
}
