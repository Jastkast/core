<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Models\Permission;
use Modules\Core\Models\Role;

class PermissionsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Model::unguard();

        Permission::createResource('dashboard users');
        Permission::createResource('dashboard users roles');
        Permission::createResource('dashboard users permissions');

        //TODO: refactor this to make super_admin capable to do anything in system
        $super_admin = Role::whereSlug('super_admin')->first();
        $super_admin->syncPermissions(Permission::all()->pluck('id')->toArray());
    }
}
