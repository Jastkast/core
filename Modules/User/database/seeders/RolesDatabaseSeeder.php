<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Models\Role;

class RolesDatabaseSeeder extends Seeder
{
    public function run() {
        Model::unguard();

        Role::create([
            'name' => 'Super Administrator',
            'slug' => 'super_admin'
        ]);

        Role::create([
            'name' => 'User',
            'slug' => 'user'
        ]);
    }
}
