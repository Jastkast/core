<?php

namespace Modules\User\Forms;

use Modules\Core\Forms\CoreForm;
use Modules\Core\Models\Permission;

class RoleForm extends CoreForm
{
    public function createForm() {
        $this->add('name', 'text');

        Permission::all()->groupBy('resource')->each(function ($resource, $key) {
            $this->add('permissions['.str_slug($key, '_').']', 'choice', [
                'choices' => $resource->pluck('name', 'id')->toArray(),
                'choice_options' => [
                    'wrapper' => ['class' => 'choice-wrapper'],
                    'label_attr' => ['class' => 'label-class'],
                ],
                'expanded' => true,
                'multiple' => true
            ]);
        });

        $this->add('submit', 'submit', ['label' => 'Save form']);
    }

    public function updateForm() {
        $this->add('name', 'text');

        Permission::all()->groupBy('resource')->each(function ($resource, $key) {
            $this->add('permissions['.str_slug($key, '_').']', 'choice', [
                'choices' => $resource->pluck('name', 'id')->toArray(),
                'choice_options' => [
                    'wrapper' => ['class' => 'choice-wrapper'],
                    'label_attr' => ['class' => 'label-class'],
                ],
                'selected' => $this->model->getPermissions()->keys()->toArray(),
                'expanded' => true,
                'multiple' => true,
                'label' => $key
            ]);
        });

        $this->add('submit', 'submit', ['label' => 'Save form']);
    }
}
