<?php

namespace Modules\User\Forms;

use Modules\Core\Forms\CoreForm;
use Modules\Core\Models\Role;

class UserForm extends CoreForm
{
    public function createForm() {
        $this->add('email', 'text')
            ->add('name', 'text')
            ->add('role_id', 'entity', [
                'class' => Role::class,
                'property' => 'name',
            ])
            ->add('password', 'repeated', [
                'type' => 'password',
                'second_name' => 'password_confirmation',
                'first_options' => ['value' => ''],
                'second_options' => [],
            ])
            ->add('submit', 'submit', ['label' => 'Save form']);
    }

    public function updateForm() {
        $this->add('email', 'text')
            ->add('name', 'text')
            ->add('role_id', 'entity', [
                'class' => Role::class,
                'property' => 'name',
                'selected' => $this->model->roles->first()->id ?? null
            ])
            ->add('password', 'repeated', [
                'type' => 'password',
                'second_name' => 'password_confirmation',
                'first_options' => ['value' => ''],
                'second_options' => [],
            ])
            ->add('submit', 'submit', ['label' => 'Save form']);
    }
}
