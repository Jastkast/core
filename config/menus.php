<?php

return [
    'styles' => [
        'dashboard_sidebar' => Modules\Dashboard\Presenters\DashboardSidebarPresenter::class
    ],
    'ordering' => false,
];
