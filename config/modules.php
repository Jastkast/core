<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Module Namespace
    |--------------------------------------------------------------------------
    |
    | Default module namespace.
    |
    */

    'namespace' => 'Modules',

    /*
    |--------------------------------------------------------------------------
    | Module Stubs
    |--------------------------------------------------------------------------
    |
    | Default module stubs.
    |
    */

    'stubs' => [
        'enabled' => true,
        'path' => base_path('Modules/Core/console/stubs/Module'),
        'files' => [
            'config/config' => 'config/config.php',
            'database/seeders/seeder' => 'database/seeders/$STUDLY_NAME$DatabaseSeeder.php',
            'database/migrations/migration'
                => 'database/migrations/'.date('Y_m_d').'_000000_create_$LOWER_NAME$_table.php',
            'datatables/datatable' => 'datatable/$STUDLY_NAME$DataTable.php',
            'forms/form' => 'forms/$STUDLY_NAME$Form.php',
            'http/controllers/controller' => 'http/controllers/$STUDLY_NAME$Controller.php',
            'http/controllers/dashboard-controller' => 'http/controllers/$STUDLY_NAME$DashboardController.php',
            'http/requests/request' => 'http/requests/$STUDLY_NAME$Request.php',
            'http/routes' => 'http/routes.php',
            'models/model' => 'models/$STUDLY_NAME$.php',
            'providers/provider' => 'providers/$STUDLY_NAME$ServiceProvider.php',
            'composer' => 'composer.json',
            'start' => 'start.php',
        ],
        'replacements' => [
            'config/config' => ['STUDLY_NAME', 'PLURAL_STUDLY_NAME', 'LOWER_NAME'],
            'database/seeders/seeder' => ['STUDLY_NAME', 'LOWER_NAME'],
            'database/migrations/migration' => ['STUDLY_NAME', 'PLURAL_NAME'],
            'datatables/datatable' => ['LOWER_NAME', 'STUDLY_NAME', 'PLURAL_NAME'],
            'forms/form' => ['STUDLY_NAME'],
            'http/controllers/controller' => ['LOWER_NAME', 'STUDLY_NAME'],
            'http/controllers/dashboard-controller' => ['LOWER_NAME', 'STUDLY_NAME', 'PLURAL_NAME'],
            'http/requests/request' => ['STUDLY_NAME'],
            'http/routes' => ['LOWER_NAME', 'STUDLY_NAME', 'PLURAL_NAME'],
            'models/model' => ['LOWER_NAME', 'STUDLY_NAME'],
            'providers/provider' => [
                'LOWER_NAME',
                'STUDLY_NAME',
                'PATH_VIEWS',
                'PATH_CONFIG',
                'PATH_LANG',
                'MIGRATIONS_PATH'
            ],
            'routes' => ['LOWER_NAME', 'STUDLY_NAME'],
            'json' => ['LOWER_NAME', 'STUDLY_NAME'],
            'scaffold/config' => ['STUDLY_NAME'],
            'composer' => [
                'LOWER_NAME',
                'STUDLY_NAME',
                'VENDOR',
                'AUTHOR_NAME',
                'AUTHOR_EMAIL',
            ],
        ],
    ],
    'paths' => [
        /*
        |--------------------------------------------------------------------------
        | Modules path
        |--------------------------------------------------------------------------
        |
        | This path used for save the generated module. This path also will be added
        | automatically to list of scanned folders.
        |
        */

        'modules' => base_path('Modules'),
        /*
        |--------------------------------------------------------------------------
        | Modules assets path
        |--------------------------------------------------------------------------
        |
        | Here you may update the modules assets path.
        |
        */

        'assets' => public_path('modules'),
        /*
        |--------------------------------------------------------------------------
        | The migrations path
        |--------------------------------------------------------------------------
        |
        | Where you run 'module:publish-migration' command, where do you publish the
        | the migration files?
        |
        */

        'migration' => base_path('database/migrations'),
        /*
        |--------------------------------------------------------------------------
        | Generator path
        |--------------------------------------------------------------------------
        | Here you may update the modules generated folder paths.
        | Set the value to false to not generate a folder.
        */
        'generator' => [
            'assets' => false,
            'config' => 'config',
            'command' => false,
            'event' => false,
            'listener' => false,
            'migration' => 'database/migrations',
            'model' => 'entities',
            'repository' => false,
            'seeder' => 'database/seeders',
            'controller' => 'http/controllers',
            'filter' => false,
            'request' => 'http/Requests',
            'provider' => 'providers',
            'lang' => 'resources/lang',
            'views' => false,
            'policies' => false,
            'rules' => false,
            'test' => 'tests',
            'jobs' => false,
            'emails' => false,
            'notifications' => false,
            'resource' => false,
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Scan Path
    |--------------------------------------------------------------------------
    |
    | Here you define which folder will be scanned. By default will scan vendor
    | directory. This is useful if you host the package in packagist website.
    |
    */

    'scan' => [
        'enabled' => false,
        'paths' => [
            base_path('vendor/*/*'),
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Composer File Template
    |--------------------------------------------------------------------------
    |
    | Here is the config for composer.json file, generated by this package
    |
    */

    'composer' => [
        'vendor' => 'johndoe',
        'author' => [
            'name' => 'John Doe',
            'email' => 'john@example.com',
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Caching
    |--------------------------------------------------------------------------
    |
    | Here is the config for setting up caching feature.
    |
    */
    'cache' => [
        'enabled' => false,
        'key' => 'laravel-modules',
        'lifetime' => 60,
    ],
    /*
    |--------------------------------------------------------------------------
    | Choose what laravel-modules will register as custom namespaces.
    | Setting one to false will require to register that part
    | in your own Service Provider class.
    |--------------------------------------------------------------------------
    */
    'register' => [
        'translations' => true,
    ],
];
