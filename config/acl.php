<?php

return [
    'role'       => \Modules\Core\Models\Role::class,
    'permission' => \Modules\Core\Models\Permission::class,
];
